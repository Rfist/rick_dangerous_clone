/**
 * Created by rfist on 12.03.2017.
 */

export default class GameModel{
    constructor() {
        this.level = 0;
        this.actorPosition = 0;
        this.state = GameModel.STATE_IDLE;
        this.crushedStones = [];
        this.prizeHasBeenTaken = false;
    }

    canPlay(){
        return this.state == GameModel.STATE_IDLE;
    }

    isAccessible(column) {
        let minValue = this.LEVELS_ARRAY[this.level][0],
            maxValue = this.LEVELS_ARRAY[this.level][1],
            nextMinValue = this.LEVELS_ARRAY[this.level + 1][0],
            nextMaxValue = this.LEVELS_ARRAY[this.level + 1][1];
        return column <= maxValue && column >= minValue &&
               column <= nextMaxValue && column >= nextMinValue;
    };

    /**
     * Расположение призана текущем уровне
     */
    getPrizePosition() {
        let x = this.START_X + this.LEVELS_ARRAY[this.level][1] * this.COLUMN_WIDTH;
        let y = this.START_Y - this.level * this.COLUMN_HEIGHT;
        return new PIXI.Point(x, y);
    }

    /**
     * размеры платформы на уровнях
     */
    get LEVELS_ARRAY () {
        return [
            [0, 19],
            [0, 19],
            [0, 18],
            [1, 19],
            [3, 19],
            [0, 18],
            [1, 15],
            [2, 19],
            [7, 19],
            [8, 19],
            [0, 18],
            [0, 9],
            [0, 8],
            [1, 8]
        ];
    }
    
    /**
     * доступные для перемещения координаты летучей мыши
     */
    get BAT_POSITIONS () {
        return [
            [450, 50],
            [350, 30],
            [400, 105],
            [470, 15],
            [490, 95]
        ];
    }

    /**
     * 
     * скорость движения летучей мыши
     */
    get BAT_MOVE_SPEED() {
        return 2;
    }

    get ACTOR_ANIMATION_SPEED() {
        return 0.2;
    }

    get ACTOR_SPEED() {
        return 2;
    }

    get STONE_SPEED() {
        return 5;
    }

    get START_X() {
        return 45;
    }

    get START_Y() {
        return 485;
    }

    get COLUMN_HEIGHT() {
        return 35;
    }

    get COLUMN_WIDTH() {
        return 25;
    }

    static get STATE_IDLE() {
        return "STATE_IDLE";
    }

    static get STATE_LOOSE() {
        return "STATE_LOOSE";
    }

    static get STATE_WIN() {
        return "STATE_WIN";
    }

    static get STATE_ACTOR_MOVING() {
        return "STATE_ACTOR_MOVING";
    }

    static get STATE_GENERATE_STONES() {
        return "STATE_GENERATE_STONES";
    }

}