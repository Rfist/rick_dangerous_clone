/**
 * Created by rfist on 06.03.2017.
 */
import React from 'react';
import ReactDOM from 'react-dom';

import Layout from './ui/Layout.js';


export default class Controls {

    constructor(el) {
        this.element = el;
        this.data = {};

        document.addEventListener('ui:update', (e) => this.updateData(e.detail.data));
        this.updateLayout();
    }

    updateData(data) {
        this.data = data;
        this.updateLayout();
    }

    updateLayout() {
        ReactDOM.render(<Layout data={this.data}/>, this.element);
    }

}