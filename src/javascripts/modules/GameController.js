/**
 * Created by rfist on 12.03.2017.
 */
import GameModel from "./GameModel"

export default class GameController{

    constructor(app, model) {
        this.app = app;
        this.model = model;
        this.app.stage.on(GameController.ON_CHANGE_CURSOR_POSITION, (data) => (this.checkCursor(data))); 
        this.app.stage.on(GameController.ON_SET_STAIR, (data) => (this.setStair(data))); 
        this.app.stage.on(GameController.ON_ACTOR_MOVING_COMPLETE, () => (this.onMovingComplete()));
        this.app.stage.on(GameController.ON_STONES_FALL_DOWN, (position) => (this.finishLevel(position)));
        this.app.stage.on(GameController.ON_PRIZE_SELECT, () => (this.selectPrize()));
    }

    selectPrize() {
        if (this.model.state == GameModel.STATE_IDLE) {
            this.model.actorPosition = this.model.LEVELS_ARRAY[this.model.level][1];
            let newY = this.model.START_Y - this.model.level * this.model.COLUMN_HEIGHT,
                newX = this.model.START_X + this.model.actorPosition * this.model.COLUMN_WIDTH;
            let newPos = new PIXI.Point(newX, newY);
            this.model.state = GameModel.STATE_ACTOR_MOVING;
            this.model.prizeHasBeenTaken = true;
            this.app.stage.emit(GameController.TAKE_PRIZE, newPos);
            console.log("take prize");
        }
    }

    finishLevel(position){
        this.model.crushedStones.push(position);
        if (this.model.crushedStones.length == 4) {
            for (let i = 0; i < this.model.crushedStones.length; i++) {
                if (this.getColumnIndex(this.model.crushedStones[i].x) == this.model.actorPosition) {
                    this.model.state = GameModel.STATE_LOOSE;
                    console.log("Game Over");
                }
            }
            if (this.model.state != GameModel.STATE_LOOSE) {
                this.model.state = GameModel.STATE_IDLE;
            }
        }
    }

    onMovingComplete() {
        if (this.model.actorPosition == this.model.LEVELS_ARRAY[this.model.level][1] && this.model.prizeHasBeenTaken) {
            this.app.stage.emit(GameController.GAME_WIN);
            this.model.state = GameModel.STATE_WIN;
            console.log("Win Game!");
        } else {
            this.generateStones();
        }
    }

    generateStones(){
        this.model.crushedStones = [];
        this.app.stage.emit(GameController.ADD_STONES, this.generateRandomPositionsByLevel(this.model.level));
        this.model.state = GameModel.STATE_GENERATE_STONES;
    }

    generateRandomPositionsByLevel(level) {
        function shuffle(array) {
            let currentIndex = array.length, temporaryValue, randomIndex;

            // While there remain elements to shuffle...
            while (0 !== currentIndex) {

                // Pick a remaining element...
                randomIndex = Math.floor(Math.random() * currentIndex);
                currentIndex -= 1;

                // And swap it with the current element.
                temporaryValue = array[currentIndex];
                array[currentIndex] = array[randomIndex];
                array[randomIndex] = temporaryValue;
            }

            return array;
        }

        let arr = [];
        for (let i = this.model.LEVELS_ARRAY[level][0]; i <=  this.model.LEVELS_ARRAY[level][1]; i++) {
            arr.push(i);
        }

        let shuffled = shuffle(arr);

        let positions = [], newX, newY = this.model.START_Y - (this.model.level * this.model.COLUMN_HEIGHT) + 5;
        for (let i = 0; i < 4; i++) {
            newX = this.model.START_X + shuffled[i] * this.model.COLUMN_WIDTH;
            positions.push(new PIXI.Point(newX, newY));
        }

        return positions;
    }
    
    setStair(data) {
        let column = this.getColumnIndex(data.x);
        let newX = this.model.START_X + column * this.model.COLUMN_WIDTH;
        let canSet = this.getRowIndex(data.y) == this.model.level &&
            this.model.canPlay() &&
            this.model.isAccessible(column);
        if (canSet) {
            this.model.level++;
            this.model.actorPosition = column;
            let newY = this.model.START_Y - this.model.level * this.model.COLUMN_HEIGHT;
            let newPos = new PIXI.Point(newX, newY);
            this.model.state = GameModel.STATE_ACTOR_MOVING;
            this.app.stage.emit(GameController.ADD_STAIR, newPos);
        }
    }
    
    checkCursor(data) {
        let column = this.getColumnIndex(data.x),
            row = this.getRowIndex(data.y);
        let newX = this.model.START_X + column * this.model.COLUMN_WIDTH;
        let newY = this.model.START_Y - row * this.model.COLUMN_HEIGHT;
        let newPos = new PIXI.Point(newX, newY);
        let isVisible = this.getRowIndex(data.y) == this.model.level &&
            this.model.canPlay() &&
            this.model.isAccessible(column);
        this.app.stage.emit(GameController.ON_SET_CURSOR, newPos, isVisible);
    }

    reset() {
        this.model.level = 0;
        this.model.state = GameModel.STATE_IDLE;
        this.model.actorPosition = 0;
        this.model.crushedStones = [];
        this.model.prizeHasBeenTaken = false;
    }

    getRowIndex (y) {
        return parseInt((this.model.START_Y - y) / this.model.COLUMN_HEIGHT);
    };

    getColumnIndex (x) {
        return parseInt((x - this.model.START_X) / this.model.COLUMN_WIDTH);
    };

    // constants
    static get ON_CHANGE_CURSOR_POSITION () {
        return "ON_CHANGE_CURSOR_POSITION";
    }

    static get ON_SET_STAIR () {
        return "ON_SET_STAIR";
    }

    static get ON_SET_CURSOR () {
        return "ON_SET_CURSOR";
    }

    static get ON_ACTOR_MOVING_COMPLETE () {
        return "ON_ACTOR_MOVING_COMPLETE";
    }

    static get ON_STONES_FALL_DOWN () {
        return "ON_STONES_FALL_DOWN";
    }

    static get ADD_STAIR () {
        return "ADD_STAIR";
    }

    static get ADD_STONES () {
        return "ADD_STONES";
    }

    static get TAKE_PRIZE () {
        return "TAKE_PRIZE";
    }
    
    static get ON_PRIZE_SELECT () {
        return "ON_PRIZE_SELECT";
    }

    static get GAME_WIN () {
        return "GAME_WIN";
    }

}