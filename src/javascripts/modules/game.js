import 'pixi.js';
import GameController from "./GameController";
import GameModel from "./GameModel";
import ActorView from "./view/ActorView";
import BGView from "./view/BackgroundView";
import BatView from "./view/BatView";
import CursorView from "./view/CursorView";
import PrizeView from "./view/PrizeView";
import StoneView from "./view/StoneView";

export default class Game {
    constructor(element) {
        this.element = element;

        this.app = new PIXI.Application(600, 500, {transparent: true});
        this.element.appendChild(this.app.view);

        /**
         * Load resources
         */
        PIXI.loader.add('atlas', 'images/atlas.json').load((loader, resources) => {
                this.init();
            }
        );

        document.addEventListener('game:new', (e) => {
            console.log("New game");
            this.reset();
        });
    }

    init() {
        console.log("Game Init");
        this.model = new GameModel();
        this.controller = new GameController(this.app, this.model);
        this.stones = [];
        this.stairs = [];
        this.crushedStones = [];
        
        // view
        this.bg = new BGView(this.app);
        this.app.stage.addChild(this.bg);

        this.bat = new BatView(this.app, this.model);
        this.app.stage.addChild(this.bat);

        this.actor = new ActorView(this.app, this.model);
        this.actor.position.x = this.model.START_X - this.model.COLUMN_WIDTH;
        this.actor.position.y = this.model.START_Y - (this.model.level * this.model.COLUMN_HEIGHT);
        this.app.stage.addChild(this.actor);

        this.prize = new PrizeView(this.app, this.model);
        this.app.stage.addChild(this.prize);
        
        this.cursor = new CursorView(this.app, this.model);
        this.cursor.position.x = this.model.START_X - this.model.COLUMN_WIDTH;
        this.cursor.position.y = this.model.START_Y - (this.model.level * this.model.COLUMN_HEIGHT);
        this.app.stage.addChild(this.cursor);

        for (let i = 0; i < 4; i++) {
            let stone = new StoneView(this.app, this.model);
            this.app.stage.addChild(stone);
            this.stones.push(stone);
        }

        // add listeners
        this.app.stage.on(GameController.ADD_STAIR, (position) => (this.onStairAdded(position)));
        this.app.stage.on(GameController.ADD_STONES, (positions) => (this.onStonesAdded(positions)));
        this.app.stage.on(GameController.ON_STONES_FALL_DOWN, (position) => (this.addCrushedStone(position)));
        this.app.stage.on(GameController.ON_ACTOR_MOVING_COMPLETE, () => (this.prize.show()));
    }

    onStonesAdded(positions) {
        for (let i = 0; i < this.stones.length; i++) {
            this.stones[i].fall(positions[i]);
        }
    }

    addCrushedStone(position){
        let crushedStone = new PIXI.Sprite(PIXI.Texture.fromFrame("stone1"));
        this.app.stage.addChild(crushedStone);
        crushedStone.position.x = position.x;
        crushedStone.position.y = position.y - crushedStone.height;
        this.crushedStones.push(crushedStone);
    }

    onStairAdded(position) {
        let stair = new PIXI.Sprite(PIXI.Texture.fromFrame("stair"));
        stair.pivot = new PIXI.Point(0, stair.height);
        stair.position.y = this.model.START_Y - ((this.model.level - 1) * this.model.COLUMN_HEIGHT);
        stair.position.x = position.x;
        this.app.stage.addChild(stair);
        this.stairs.push(stair);
        // moving actor on top
        this.app.stage.addChild(this.actor);
    }

    /**
     * Reset and start new game
     */
    reset() {
        this.controller.reset();

        this.prize.reset();
        
        this.actor.reset();
        this.actor.position.x = this.model.START_X - this.model.COLUMN_WIDTH;
        this.actor.position.y = this.model.START_Y - (this.model.level * this.model.COLUMN_HEIGHT);
        
        let i;
        for (i = 0; i < this.stones.length; i++) {
            this.stones[i].reset();
        }

        for (i = 0; i < this.crushedStones.length; i++) {
            this.crushedStones[i].destroy();
        }
        this.crushedStones = [];

        for (i = 0; i < this.stairs.length; i++) {
            this.stairs[i].destroy();
        }

        this.stairs = [];
    }
}