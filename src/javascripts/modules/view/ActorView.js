/**
 * Created by rfist on 12.03.2017.
 */
import { Sprite } from 'pixi.js';
import GameController from './../GameController';
import GameModel from './../GameModel';

export default class ActorView extends Sprite {

    /**
     *
     * @param app PIXI.Application
     * @param model GameModel
     */
    constructor(app, model) {
        super();
        this.app = app;
        this.model = model;
        this.initAnimations();
        this.setAnimation("init");
        this.app.stage.on(GameController.ADD_STAIR, (position) => (this.setNewPosition(position)));
        this.app.stage.on(GameController.TAKE_PRIZE, (position) => (this.setNewPosition(position)));
        this.app.ticker.add((delta) => {
            this.checkPosition(delta);
        });
    }

    initAnimations() {
        let i,
            initAnimationFrames = [],
            idleAnimationFrames = [],
            leftAnimationFrames = [],
            rightAnimationFrames = [],
            upAnimationFrames = [],
            dieAnimationFrames = [];

        for (i = 0; i <= 2; i++) {
            leftAnimationFrames.push(PIXI.Texture.fromFrame('left' + i));
            rightAnimationFrames.push(PIXI.Texture.fromFrame('right' + i));
            upAnimationFrames.push(PIXI.Texture.fromFrame('up' + i));
        }

        initAnimationFrames.push(PIXI.Texture.fromFrame('right0'));
        idleAnimationFrames.push(PIXI.Texture.fromFrame('up0'));
        dieAnimationFrames.push(PIXI.Texture.fromFrame('die'));

        this.initAnimation =  new PIXI.extras.AnimatedSprite(initAnimationFrames);
        this.idleAnimation =  new PIXI.extras.AnimatedSprite(idleAnimationFrames);
        this.leftAnimation =  new PIXI.extras.AnimatedSprite(leftAnimationFrames);
        this.rightAnimation =  new PIXI.extras.AnimatedSprite(rightAnimationFrames);
        this.upAnimation =  new PIXI.extras.AnimatedSprite(upAnimationFrames);
        this.dieAnimation =  new PIXI.extras.AnimatedSprite(dieAnimationFrames);
    }

    // TODO: вынести в константы названия анимаций
    setAnimation(animation_name) {
        this.removeChild(this.anim);
        switch (animation_name) {
            case "init":
                this.anim = this.initAnimation;
                break;
            case "left":
                this.anim = this.leftAnimation;
                break;
            case "right":
                this.anim = this.rightAnimation;
                break;
            case "up":
                this.anim = this.upAnimation;
                break;
            case "idle":
                this.anim = this.idleAnimation;
                break;
            case "die":
                this.anim = this.dieAnimation;
                break;
        }
        this.anim.name = animation_name;
        this.anim.animationSpeed = this.model.ACTOR_ANIMATION_SPEED;
        this.anim.pivot = new PIXI.Point(0, this.anim.height - 5);
        this.addChild(this.anim);
        this.anim.play();
    }
    
    reset(){
        this.setAnimation("init");
        this.anim.visible = true;
    }

    setNewPosition(position) {
        this.newPosition = position;
        this.newPosition.x > this.anim.parent.position.x ? this.setAnimation("right") : this.setAnimation("left");
    }

    checkPosition(delta) {
        if (this.model.state == GameModel.STATE_ACTOR_MOVING && this.newPosition !== undefined) {
            if (Math.abs(this.newPosition.x - this.anim.parent.position.x) > this.model.ACTOR_SPEED) {
                if (this.anim.parent.position.x < this.newPosition.x) {
                    this.anim.parent.position.x += this.model.ACTOR_SPEED;
                } else {
                    this.anim.parent.position.x -= this.model.ACTOR_SPEED;
                }
            } else if (Math.abs(this.newPosition.y - this.anim.parent.position.y) > this.model.ACTOR_SPEED) {
                if (this.anim.name != "up") {
                    this.setAnimation("up");
                }
                if (this.anim.parent.position.y < this.newPosition.y) {
                    this.anim.parent.position.y += this.model.ACTOR_SPEED;
                } else {
                    this.anim.parent.position.y -= this.model.ACTOR_SPEED;
                }
            } else {
                if (this.anim.name != "idle") {
                    this.setAnimation("idle");
                }
                this.app.stage.emit(GameController.ON_ACTOR_MOVING_COMPLETE);
            }
        }
        if (this.model.state == GameModel.STATE_LOOSE && this.anim.parent.position.y < this.model.START_Y + 50) {
            if (this.anim.name != "die") {
                this.setAnimation("die");
                this.anim.pivot = new PIXI.Point(this.anim.width / 2, this.anim.height / 2);
            }
            this.anim.rotation += 0.1 * delta;
            this.anim.parent.position.y += this.model.ACTOR_SPEED;
        }
        this.anim.visible = this.model.state != GameModel.STATE_WIN;
    }
}