/**
 * Created by rfist on 12.03.2017.
 */
import { Sprite } from 'pixi.js';
import GameController from './../GameController';
import GameModel from './../GameModel';

export default class StoneView extends Sprite {

    /**
     *
     * @param app PIXI.Application
     * @param model GameModel
     */
    constructor(app, model) {
        super();
        this.app = app;
        this.model = model;
        this.stone = new PIXI.Sprite(PIXI.Texture.fromFrame("stone"));
        this.stone.pivot = new PIXI.Point(0, this.stone.height);
        this.stone.visible = false;
        this.addChild(this.stone);

        this.app.ticker.add(() => {
            this.checkPosition();
        });
    }
    
    fall(targetPosition) {
        this.stone.visible = true;
        this.stone.parent.y = 5;
        this.stone.parent.x = targetPosition.x;
        this.targetPosition = targetPosition;
    }

    checkPosition() {
        if (this.model.state == GameModel.STATE_GENERATE_STONES && this.targetPosition !== undefined) {
            if (this.targetPosition.y - this.stone.parent.position.y > this.model.STONE_SPEED) {
                this.stone.parent.position.y += this.model.STONE_SPEED;
            } else {
                this.stone.visible = false;
                this.app.stage.emit(GameController.ON_STONES_FALL_DOWN, this.targetPosition);
            }
        }
    }
    
    reset() {
        this.stone.visible = false;
    }
}