/**
 * Created by rfist on 12.03.2017.
 */
import { Sprite } from 'pixi.js';

export default class BatView extends Sprite{

    /**
     *
     * @param app PIXI.Application
     * @param model GameModel
     */
    constructor (app, model){
        super();

        this.app = app;
        this.model = model;

        let frames = [];

        for (var i = 0; i <= 1; i++) {
            frames.push(PIXI.Texture.fromFrame('bat' + i));
        }

        this.bat = new PIXI.extras.AnimatedSprite(frames);

        this.bat.animationSpeed = 0.2;
        this.bat.play();
        this.bat.interactive = true;
        this.bat.currentPosition = 0;
        this.bat.positions = this.model.BAT_POSITIONS;
        this.bat.position.x = this.bat.newX = this.bat.positions[this.bat.currentPosition][0];
        this.bat.position.y = this.bat.newY = this.bat.positions[this.bat.currentPosition][1];
        this.addChild(this.bat);
        this.bat.on('mouseover', this.onMouseOver);

        this.app.ticker.add(() => {
            this.checkPosition();
        });
    }

    checkPosition() {
        if (Math.abs(this.bat.newX - this.bat.position.x) > this.model.BAT_MOVE_SPEED) {
            if (this.bat.position.x < this.bat.newX) {
                this.bat.position.x += this.model.BAT_MOVE_SPEED;
            } else {
                this.bat.position.x -= this.model.BAT_MOVE_SPEED;
            }
        }
        if (Math.abs(this.bat.newY - this.bat.position.y) > this.model.BAT_MOVE_SPEED) {
            if (this.bat.position.y < this.bat.newY) {
                this.bat.position.y += this.model.BAT_MOVE_SPEED;
            } else {
                this.bat.position.y -= this.model.BAT_MOVE_SPEED;
            }
        }
    }

    onMouseOver(){
        this.currentPosition = this.currentPosition == this.positions.length - 1 ? 0 : this.currentPosition + 1;
        this.newX = this.positions[this.currentPosition][0];
        this.newY = this.positions[this.currentPosition][1];
    }
}