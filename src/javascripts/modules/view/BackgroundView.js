/**
 * Created by rfist on 12.03.2017.
 */
import { Sprite } from 'pixi.js';
import GameController from './../GameController';

export default class BGView extends Sprite{
    constructor (app){
        super();
        this.app = app;
        let backgroundTexture = PIXI.Texture.fromImage('images/back.jpg');
        this.bg = new PIXI.Sprite(backgroundTexture);
        this.bg.interactive = true;
        this.addChild(this.bg);
        this.bg.on('mouseup', (e) => this.onMouseUp(e))
               .on('mousemove',  (e) => this.onMouseMove(e));

    }
    
    onMouseUp(e){
        let pos = e.data.getLocalPosition(this.bg);
        this.app.stage.emit(GameController.ON_SET_STAIR, pos);
    }

    onMouseMove(e){
        let pos = e.data.getLocalPosition(this.bg);
        this.app.stage.emit(GameController.ON_CHANGE_CURSOR_POSITION, pos);
    }
}