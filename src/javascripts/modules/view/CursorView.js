/**
 * Created by rfist on 12.03.2017.
 */
import { Sprite } from 'pixi.js';
import GameController from './../GameController';

export default class CursorView extends Sprite {

    /**
     *
     * @param app PIXI.Application
     * @param model GameModel
     */
    constructor(app, model) {
        super();
        this.app = app;
        this.cursor = new PIXI.Sprite(PIXI.Texture.fromFrame("stair"));
        this.cursor.pivot = new PIXI.Point(0, this.cursor.height);
        this.cursor.visible = false;
        this.addChild(this.cursor);
        this.app.stage.on(GameController.ON_SET_CURSOR, (position, visibility) => (this.setCursor(position, visibility)));
    }
    
    setCursor(position, visibility) {
        this.cursor.parent.position = position;
        this.cursor.visible = visibility;
    }
}