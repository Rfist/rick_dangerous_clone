/**
 * Created by rfist on 12.03.2017.
 */
import { Sprite } from 'pixi.js';
import GameController from './../GameController';

export default class CursorView extends Sprite {

    /**
     *
     * @param app PIXI.Application
     * @param model GameModel
     */
    constructor(app, model) {
        super();
        this.app = app;
        this.model = model;

        this.prize = new PIXI.Sprite(PIXI.Texture.fromFrame("diamond0"));
        this.prize.pivot = new PIXI.Point(0, this.prize.height);
        this.prize.visible = false;
        this.prize.interactive = true;
        this.addChild(this.prize);

        this.prize.on('mouseup', (e) => this.app.stage.emit(GameController.ON_PRIZE_SELECT));
        this.app.stage.on(GameController.GAME_WIN, () => (this.take()));
    }

    show() {
        this.prize.visible = true;
        this.prize.parent.position = this.model.getPrizePosition();
    }

    reset() {
        this.prize.texture = PIXI.Texture.fromFrame("diamond0");
        this.prize.visible = false;
    }

    take() {
        this.prize.texture = PIXI.Texture.fromFrame("win");
    }
}