
#Social engine

###Installation:
At first you need to have **node.js**and**npm**

Then run in the terminal:
```
npm install
```
###Development:
```
npm run development
```

###Used technologies

* [Starter kit](https://github.com/vigetlabs/gulp-starter)
* [React.js](https://facebook.github.io/react/)
* [PixiJS](http://www.pixijs.com/)
